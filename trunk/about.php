<?php
	$page_title = 'About';
	include('templates/header.php');
?>
<section class="page-about">
  <h1>About Us. Our Mission. Shortcodes. Tons More!</h1>
  <p class="about-intro">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis.
  </p>
  <figure class="left-positioned">
    <img src="img/articles/img_post4.png" width="212" height="211" alt="about" />
  </figure>
  <p>
    Maecenas ipsum metus, semper hendrerit varius mattis, hasr congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elem entum enim rutrum. Nam mi erat, porta idso ultrices nec, pellentesq ue vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdumni justo ultricies. Sed luctus dui nec ni  sl tem pus faucibus sit amet et sem. Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae.<br />
  </p>
  <p>
    Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci.  Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros. Maecenas ipsum metus, semper hendrerit varius mattis, congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elementum enim rutrum. Nam mi erat, porta id ultrices nec, pellentesque vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdum.
  </p>
  <p>
    Maecenas ipsum metus, semper hendrerit varius mattis, congue sit amet tellus. Aliquam ullamcorper dui sed magna posuere ut elementum enim rutrum. Nam mi erat, porta id ultrices nec, pellentesque vel nunc. Cras varius fermentum iaculis. Aenean sodales nibh non lectus tempor a interdum justo ultricies. Sed luctus dui nec nisl tempus faucibus sit amet et sem. Aenean augue sapien, sodales ac bibendum ut, pellentesque id eros.
  </p>
  <p>
    Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae.
  </p>
  <p class="capital-letter">
    DQuisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed l           	         ectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Maecenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Duis tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. Pellentesque ultricies nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci.
  </p>
  <blockquote class="quote">
    Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem. Mae trenas in magna mollis lectus lacinia mollis. Donec sit amet volutpat lorem. Du tristique porta aliquam. Curabitur sagittis tincidunt erat, quis hendrerit nibh iaculis vitae. Pellentesque ultric nisl quis odio posuere facilisis. In ut felis erat, ac laoreet orci donec sed nulla at sit amet.
  </blockquote>
  <div class="grid-row row-cells-2 clearfix">
    <p class="col-1">
      Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem.
    </p>
    <p class="col-1">
      Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas. Sed lectus nulla, bibendum at vulputate sit amet, tincidunt volutpat lorem.
    </p>
  </div>
  <div class="grid-row row-cells-3 clearfix">
    <p class="col-1">
      Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.
    </p>
    <p class="col-1">
      Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.
    </p>
    <p class="col-1">
      Quisque mollis, sem id laoreet pretium, lectus elit molestie urna, id tristique risus ante at est. Sed pretium metus sit amet erat blandit vitae pulvinar lorem egestas.
    </p>
  </div>
  <div class="clearfix">
    <button class="button button-green">Button Text</button>
    <button class="button button-silver">Button Text</button>
    <button class="button button-blue">Button Text</button>
    <button class="button button-orange">Button Text</button>
    <button class="button button-purple">Button Text</button>
    <button class="button button-red">Button Text</button>
    <button class="button button-baby-blue">Button Text</button>
  </div>
  <div class="clearfix">
    <div class="alert alert-green">Alert Text</div>
    <div class="alert alert-red">Alert Text</div>
    <div class="alert alert-orange">Alert Text</div>
    <div class="alert alert-blue">Alert Text</div>
  </div>
</section>
<?php include('templates/footer.php') ?>>