<?php
    $page_title = 'Contacts';
    include('templates/header.php');
?>
<section>
  <h1>Contact Us. Locate Us. Send Us Your Thoughts!</h1>
  <p class="contacts-information">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Maecenas in magna mollis lectus lacinia mollis.
  </p>
  <div id="map-canvas" style="height: 300px; width: 100%;"></div>
  <form id="formContacts" class="contacts-form">
    <input type="text" placeholder="FIRST NAME" id="txtContactsFirstName" name="FirstName" />
    <input type="text" placeholder="LAST NAME" id="txtContactsLastName" name="LastName" />
    <input type="text" placeholder="EMAIL ADDRESS" id="txtContactsEmailAddress" name="EmailAddress" />
    <textarea placeholder="MESSAGE" id="taContactsMessage" name="Message"></textarea>
    <button type="submit" class="right-arrow link-more">Submit</button>
  </form>
</section>
<?php include('templates/footer.php') ?>