<?php include '/inc/env.php'; ?>
<!DOCTYPE html>
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="no-js eq-ie9 ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="<?php echo $basehref; ?>">
    <title><?php echo $page_title; ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php if($page_title == 'Contacts') { ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXSvNhrGGECmJMZKs3Ewbz32jVRvLTTq4&language=en"></script>
    <?php } ?>
    <script type="text/javascript">
        var currentPage = '<?php echo $page_title; ?>';
		var baseHref = '<?php echo $basehref; ?>';
    </script>
</head>
<body>
    <div class="wrapper clearfix" role="main">
        <header>
            <div class="head-wrapper clearfix">
				<div class="logo-holder">
					<a href="<?php echo $basehref; ?>" class="logo no-decoration">Gridzilla<span class="logo-circle">.</span></a>
				</div>
				<form class="search-form">
					<input type="text" placeholder="Enter your search ..." id="txtSearchPhrase" name="SearchPhrase" />
					<button type="submit" id="btnSubmitSearch">
						<i class="fa fa-search"></i>
					</button>
				</form>
			</div>
			<nav class="main-navigation clearfix" id="siteMainNav">
				<a href="index.php" class="no-decoration">Home</a>
				<a href="about.php" class="no-decoration">About</a>
				<a href="blog.php" class="no-decoration">Blog</a>
				<a href="contacts.php" class="no-decoration">Contact</a>
			</nav>
        </header>