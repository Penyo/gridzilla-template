<?php
	$page_title = 'Home';
	include('templates/header.php');
?>
<div class="carousel">
  <div class="slider">
    <div class="slide clearfix active">
      <a href="article.php?aid=1" class="slide-size-1 slide-el-top slide-el-left article-add-info">
        <img src="img/articles/slider/slider-post-1.png" alt="Article's Name" width="307" height="74" />
      </a>
      <a href="article.php?aid=1" class="slide-size-1 slide-size-longer slide-el-top article-add-info">
        <img src="img/articles/slider/slider-post-2.png" alt="Article's Name" width="314" height="74" />
      </a>
      <a href="article.php?aid=1" class="slide-size-1 slide-el-top slide-el-right article-add-info">
        <img src="img/articles/slider/slider-post-3.png" alt="Article's Name" width="307" height="74" />
      </a>
      <div class="slide-size-4 slide-el-left">
        <a href="article.php?aid=1">
          <img src="img/articles/slider/slider-post-4.png" alt="Article's Name" width="627" height="299" />
        </a>
        <div class="slide-centered">
          <h1>
            <a>Top iPhone Apps</a>
          </h1>
          <p>Lorem ipsum dolor sit amet, ne utinam discere blandit vim, at iusto facilisis mel. Cetero audire sea an, has ex quem prima omnium.</p>
          <a href="article.php?aid=1" class="no-decoration more right-arrow-white">More</a>
        </div>
      </div>
      <a href="article.php?aid=1" class="slide-size-2 slide-el-right article-add-info">
        <img src="img/articles/slider/slider-post-5.png" alt="Article's Name" width="307" height="183" />
      </a>
      <a href="article.php?aid=1" class="slide-size-3 slide-el-right article-add-info">
        <img src="img/articles/slider/slider-post-6.png" alt="Article's Name" width="307" height="110" />
      </a>
      <a href="article.php?aid=1" class="slide-size-5 slide-el-left slide-el-bottom article-add-info">
        <img src="img/articles/slider/slider-post-7.png" alt="Article's Name" width="307" height="116" />
      </a>
      <a href="article.php?aid=1" class="slide-size-5 slide-size-longer slide-el-bottom article-add-info">
        <img src="img/articles/slider/slider-post-8.png" alt="Article's Name" width="314" height="116" />
      </a>
      <a href="article.php?aid=1" class="slide-size-5 slide-el-right slide-el-bottom article-add-info">
        <img src="img/articles/slider/slider-post-9.png" alt="Article's Name" width="307" height="116" />
      </a>
    </div>
    <div class="slide clearfix simple-slide">
      <div class="slide-size-1 slide-el-top slide-el-left"></div>
      <div class="slide-size-1 slide-el-top"></div>
      <div class="slide-size-1 slide-el-top slide-el-right"></div>
      <div class="slide-size-4 slide-el-left"></div>
      <div class="slide-size-3 slide-el-right"></div>
      <div class="slide-size-2 slide-el-right"></div>
      <div class="slide-size-5 slide-el-left slide-el-bottom"></div>
      <div class="slide-size-5 slide-el-bottom"></div>
      <div class="slide-size-5 slide-el-right slide-el-bottom"></div>
      <div class="slide-centered"></div>
    </div>
  </div>
  <div class="controls">
    <button class="slide-left" title="Previous slide"></button>
    <button class="slide-right" title="Next slide"></button>
  </div>
</div>
<?php
  $blog_title = "A Theme Unlike Any Other. Simply Fantastic!";
  include('blog_content.php');
  include('templates/footer.php');
?>