<?php
	$page_title = 'Article Name';
	include('templates/header.php');
?>
<section class="article-content clearfix">
  <h1>Sticker Mule. Best Place For Your Sticker Needs!</h1>
  <div class="article-images">
    <img src="img/articles/article-1/blog_item_page.png" alt="Alternate text based on article's name" width="540" height="400" />
    <img src="img/articles/article-1/blog_item_page2.png" alt="Alternate text based on article's name" width="540" height="400" />
  </div>
  <div class="article-data">
    <div class="grid-row row-cells-3 clearfix article-stats-header">
      <div class="col-1">Date</div>
      <div class="col-1">Tags</div>
      <div class="col-1">Author</div>
    </div>
    <div class="grid-row row-cells-3 clearfix article-stats">
      <div class="col-1">April 15, 2012</div>
      <div class="col-1">Website | Design</div>
      <div class="col-1">Michael Reimer</div>
    </div>
    <p>
      Lorem ipsum dolor sit amet, mollis epicuri pri ei, perpetua honestatis ad vix. Ne duo ludus putent, cu causae tamquam voluptua duo. Agam officiis no duo, ut reque decore sea. Cu eripuit accusam vix. Facete blandit detraxit pri cu, sea soluta doming civibus ea.      
    </p>
    <p>
      Eum eu tale clita iuvaret, cu est saperet forensibus interesset, cum ne case iusto oportere. Id idque indoctum eum, menandri mediocrem has ei. At usu modo quaerendum. Sit ei dicunt tacimates, mea ea enim eirmod suscipiantur, amet dicit ancillae vel in. Ex mea augue eloquentiam, his postea dolorem suavitate ea. Mel hendrerit accommodare concludaturque ex.
    </p>
    <div>
      <a class="no-decoration right-arrow link-more" href="https://google.com" target="_blank">Visit Website</a>
    </div>
  </div>
</section>
<aside class="similar-posts">
  <h1>Similar Posts. Check Them Out!</h1>
  <div class="clearfix articles-container">
    <div class="articles-column">
      <article>
        <figure>
          <a href="article.php?aid=6">
            <img src="img/articles/img_post6.png" alt="Character Design" />
          </a>
        </figure>
        <div class="article-content">
          <h2>
            <a href="article.php?aid=6">Character Design</a>
          </h2>
          <h3>June 15, 2012</h3>
        </div>
      </article>
    </div>
    <div class="articles-column">
      <article>
        <figure>
          <a href="article.php?aid=3">
            <img src="img/articles/img_post3.png" alt="Top iPhone Apps" />
          </a>
        </figure>
        <div class="article-content">
          <h2>
            <a href="article.php?aid=3">Top iPhone Apps</a>
          </h2>
          <h3>June 15, 2012</h3>
        </div>
      </article>
    </div>
    <div class="articles-column">
      <article>
        <figure>
          <a href="article.php?aid=11">
            <img src="img/articles/img_post11_small.png" alt="Social Media Buttons" />
          </a>
        </figure>
        <div class="article-content">
          <h2>
            <a href="article.php?aid=11">Social Media Buttons</a>
          </h2>
          <h3>June 15, 2012</h3>
        </div>
      </article>
    </div>
    <div class="articles-column">
      <article>
        <figure>
          <a href="article.php?aid=2">
            <img src="img/articles/img_post2_small.png" alt="10 Amazing Websites" />
          </a>
        </figure>
        <div class="article-content">
          <h2>
            <a href="article.php?aid=2">10 Amazing Websites</a>
          </h2>
          <h3>June 15, 2012</h3>
        </div>
      </article>
    </div>
  </div>
</aside>
<?php include('templates/footer.php') ?>