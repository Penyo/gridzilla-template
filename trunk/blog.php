<?php
	$page_title = 'Blog';
	include('templates/header.php');
  $blog_title = "Read Our Latest Posts. Learn Something New Maybe!";
	include('blog_content.php');
	include('templates/footer.php');
?>