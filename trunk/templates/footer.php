        <footer class="clearfix">
          <div class="socials-container clearfix">
            <a class="social twitter" href="https://twitter.com" target="_blank"></a>
            <a class="social rss" href="<?php echo $basehref; ?>rss" target="_blank"></a>
            <a class="social facebook " href="https://facebook.com" target="_blank"></a>
            <a class="social pinterest" href="https://pinterest.com" target="_blank"></a>
          </div>
          <div class="copyright">
            <p>
              <span id="ownerContacts">
                Best PSD Freebies | <a href="mailto:info@bestpsdfreebies.com" class="no-decoration">info@bestpsdfreebies.com</a>
              </span>
              <br />
              <span id="ownerInfo">
                Copyright 2012 Gridzilla Theme <a href="http://www.bestpsdfreebies.com" class="no-decoration">www.bestpsdfreebies.com</a>
              </span>
            </p>
          </div>
        </footer>
    </div><!-- End of wrapper -->
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script>
        if (typeof jQuery == 'undefined') {
            var e = document.createElement('script');
            e.src = "js/libs/jquery-1.8.3.min.js";
            e.type='text/javascript';
            document.getElementsByTagName("head")[0].appendChild(e);
        }
    </script>
    <script src="js/application.js"></script>
</body>
</html>