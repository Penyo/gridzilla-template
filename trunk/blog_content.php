<section>
    <h1 class="site-title"><?php echo $blog_title; ?></h1>
    <div class="categories">
        <ul class="clearfix">
            <li class="active" data-category="0">All</li>
            <li data-category="1">News</li>
            <li data-category="2">Design</li>
            <li data-category="3">Print</li>
            <li data-category="4">Art</li>
            <li data-category="5">Development</li>
        </ul>
    </div>
    <div class="articles-container clearfix" id="dvArticles">
        <div class="articles-column">
            <article data-aid="1">
                <div class="article-add-info">
                    <img src="img/articles/img_post1.png" alt="Sticker Mule" width="220" height="223" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>857</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>07/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>588</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Sticker Mule</h1>
                    <h2>June 15, 2012<span class="categories">news, contests</span>
                    </h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=1" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="5">
                <div class="article-add-info">
                    <img src="img/articles/img_post5.png" alt="Big Buck Bunny" width="220" height="293" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Big Buck Bunny</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=5" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="9">
                <div class="article-add-info">
                    <img src="img/articles/img_post9.png" alt="Pinterest Icons" width="220" height="183" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Pinterest Icons</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=9" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="articles-column">
            <article data-aid="2">
                <div class="article-add-info">
                    <img src="img/articles/img_post2.png" alt="10 Amazing Websites" width="220" height="293" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>10 Amazing Websites</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=2" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="6">
                <div class="article-add-info">
                    <img src="img/articles/img_post6.png" alt="Character Design" width="220" height="183" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Character Design</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=6" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="10">
                <div class="article-add-info">
                    <img src="img/articles/img_post10.png" alt="iPad 3 Review" width="220" height="293" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>iPad 3 Review</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=10" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="articles-column">
            <article data-aid="3">
                <div class="article-add-info">
                    <img src="img/articles/img_post3.png" alt="Top iPhone Apps" width="220" height="183" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Top iPhone Apps</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=3" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="7">
                <div class="article-add-info">
                    <img src="img/articles/img_post7.png" alt="Service Icons" width="220" height="223" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Service Icons</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=7" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="11">
                <div class="article-add-info">
                    <img src="img/articles/img_post11.png" alt="Social Media Buttons" width="220" height="234" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Social Media Buttons</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=11" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
        </div>
        <div class="articles-column">
            <article data-aid="4">
                <div class="article-add-info">
                    <img src="img/articles/img_post4.png" alt="Photo Shoot" width="220" height="223" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Photo Shoot</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=4" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="8">
                <div class="article-add-info">
                    <img src="img/articles/img_post8.png" alt="Wedding" width="220" height="223" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Wedding </h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=8" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
            <article data-aid="12">
                <div class="article-add-info">
                    <img src="img/articles/img_post12.png" alt="Silver UI Kit" width="220" height="269" />
                    <div class="add-info-data">
                        <div>
                            <span class="fa fa-eye"></span>
                            <span>124</span>
                        </div>
                        <div>
                            <span class="fa fa-clock-o"></span>
                            <span>17/05/2012</span>
                        </div>
                        <div>
                            <span class="fa fa-heart"></span>
                            <span>123</span>
                        </div>
                    </div>
                </div>
                <div class="article-content">
                    <h1>Silver UI Kit</h1>
                    <h2>June 15, 2012</h2>
                    <p>Lorem ipsum dolor sit amet, te possim inimicus ius. Alii ullam at corper pri ad, per nulla luptatum te, in qui delenit nostrum. Nam ad labores.</p>
                    <div>
                        <a href="article.php?aid=12" class="right-arrow more no-decoration">More</a>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <nav class="articles-nav">
        <button class="articles-nav-button">1</button>
        <button class="articles-nav-button active">2</button>
        <button class="articles-nav-button">3</button>
        <button class="articles-nav-button">4</button>
    </nav>
</section>