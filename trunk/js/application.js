/**
 * Master application
 * Includes Classes Env, App
 */

window.App = window.App || {};
window.Env = window.Env || {};

(function($, App, Env){

    /**
     * Document ready
     */
    $(function() {

        switch (currentPage) {
            case 'Contacts':
            break;
        }

        /*-------------------------------------------------------
        Google Maps Initialization
        -------------------------------------------------------*/

        function initializeContacts() {
            var mapOptions = {
                center: new google.maps.LatLng(42.687611, 23.335194),
                zoom: 17,
                panControl: false,
                zoomControl: true,
                scaleControl: true,
                scrollwheel: true,
                mapTypeControl: false,
                disableDoubleClickZoom: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        }
        
        function addArticlesEvents() {
            $(document).on("click", "#dvArticles article", function () {
                var $article = $(this);
                var id = $article.data("aid");
                location.href = "article.php?aid=" + id;
            });
        }

        function MarkCurrentPageAsSelected() {
            var page = location.href.replace(baseHref, "");
            if (page == "") {
                page = baseHref;
            }
            else if (page.toLowerCase().indexOf("article.php") > -1) { //hardcoded, should has any business logic for this.
                page = "blog.php";
            }
            $("#siteMainNav a[href='" + page + "']").addClass("current").siblings().removeClass("current");
        }

        App.AddClassToWrapper = function(className) {
            $(".wrapper").addClass(className);
        }

        if (currentPage == 'Contacts') {
            initializeContacts();
        }
        else if (["home", "blog"].indexOf(currentPage.toLowerCase()) > -1) {
            addArticlesEvents();
        }
        MarkCurrentPageAsSelected();
    });

})($, App, Env);